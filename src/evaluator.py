import streamlit as st
import requests
import json
import numpy as np
import seaborn as sns

import os
import base64
import time
import pandas as pd

from vecto.benchmarks.categorization.categorization import KMeansCategorization
from vecto.benchmarks.outliers.outliers import AveragePairwiseCosine
from vecto.benchmarks.synonymy_detection import CosineDistance
import vecto.embeddings


def get_binary_file_downloader_html(bin_file, file_label='File'):
    with open(bin_file, 'rb') as f:
        data = f.read()
    bin_str = base64.b64encode(data).decode()
    href = f'<a href="data:application/octet-stream;base64,{bin_str}" download="{os.path.basename(bin_file)}">{file_label}</a>'
    return href


def main():
    hide_streamlit_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
    st.markdown(hide_streamlit_style, unsafe_allow_html=True) 
    
    st.subheader("Distributional Semantic Models Evaluation Platform")
    st.write("*Funded by the Russian Foundation for Basic Research project 20-37-90153*")
    st.markdown('''
        **What you can do here:** 
        1. Download the datasets. 
        2. Evaluate the model online.
    ''')

    tab1, tab2 = st.tabs(["Download datases", "Evaluate online"])

    with tab1:

        st.info('For more information about the datasets refer to https://arxiv.org/abs/1801.09536')

        evaliation_tasks = os.listdir('data')
        task_type = st.selectbox('Select evaluation task:', evaliation_tasks, index=0)
        available_languages = os.listdir('data/{}/monolingual'.format(task_type))
        language = st.selectbox('Select language:', available_languages)
        available_datasets = os.listdir('data/{}/monolingual/{}'.format(task_type, language))
        selected_dataset = st.selectbox('Select dataset:', available_datasets)
        with open(os.path.join('data/{}/monolingual/{}'.format(task_type, language), selected_dataset), 'r') as f:
            st.download_button('Download {}'.format(selected_dataset), f, file_name=selected_dataset)

    with tab2:
        model_type = st.radio("Choose the model", ["Demo model (GloVe)", "Upload your own"])
        model_name = "./models/glove.6B.50d.txt"
        if model_type == "Demo model (GloVe)":
            st.info("The model is `glove.6B.50d.txt` from https://nlp.stanford.edu/projects/glove/")
        else:
            model = st.file_uploader("Choose a distributional semantic model...", type="txt")
        
            if model is not None:
                with open('./models/custom_model.txt', 'wb') as f:
                    f.write(model.getvalue())
            model_name = './models/custom_model.txt'

        evaliation_tasks = ['word-categorization', 'outlier-detection', 'word-similarity']
        task_type = st.selectbox('Select evaluation task', evaliation_tasks, index=0)
        available_languages = os.listdir('data/{}/monolingual'.format(task_type))
        language = st.selectbox('Select language', available_languages)
        
        if task_type == 'word-categorization':
            available_datasets = os.listdir('data/{}/monolingual/{}'.format(task_type, language))
            selected_dataset = st.selectbox('Select dataset', available_datasets)

        button = st.button("Evaluate")

        if button:
            with st.spinner("Evaluating..."):
                embeddings = vecto.embeddings.load_from_file(model_name)
                if task_type == 'outlier-detection':
                    outliers = AveragePairwiseCosine()
                    result = outliers.get_result(embeddings, 'data/{}/monolingual/{}'.format(task_type, language))
                elif task_type == 'word-similarity':
                    dist = CosineDistance()
                    result = dist.get_result(embeddings, 'data/{}/monolingual/{}'.format(task_type, language))
                elif task_type == 'word-categorization':
                    categorization = KMeansCategorization()
                    result = categorization.run_on_single_dataset(embeddings, 'data/{}/monolingual/{}/{}'.format(task_type, language, selected_dataset))
                result_json = json.dumps(result, indent=4)
                st.download_button('Download the Result', result_json, file_name='result.json')
                st.write(result)

if __name__ == "__main__":
    main()
