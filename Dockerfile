FROM python:3.10

COPY src/ src/
WORKDIR /src

RUN pip install -r requirements.txt

WORKDIR /src/vecto
RUN pip install -r requirements.txt
RUN pip install -U .

WORKDIR /src

ENV PORT 8501

CMD streamlit run evaluator.py --server.port 8080