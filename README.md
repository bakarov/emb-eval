# Word Embeddings Evaluation

Project supported by RBFR 20-37-90153 

How to run:

1. Clone this repo
2. Clone `https://github.com/vecto-ai/benchmarks.git` to the same directory.
3. Install requirement file (`pip -r install requirements.txt`)
4. `cd Streamlit && streamlit run evaluator.py`